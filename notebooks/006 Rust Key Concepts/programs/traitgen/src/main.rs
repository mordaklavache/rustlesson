fn main() {
    // [code...]

    let p1 = Pair {
        a: 42,
        b: 3.14,
    };
    let p2 = Pair {
        a: 42,
        b: 5.12,
    };
    dbg!(p1.compare(&p2));
    dbg!(p1.is_equal(&p2));
}

