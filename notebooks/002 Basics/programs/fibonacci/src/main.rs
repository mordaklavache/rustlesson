fn fibonacci(num: u32) -> u32 {
    0
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    match args.get(1) {
        Some(arg) => {
            let arg = arg.parse::<u32>().unwrap();
            // Calculer ici la fibonacci du nombre n donne en parametre, et l'afficher via println!
            println!("{}", fibonacci(arg));
        }
        None => {
            eprintln!("Aucun argument !");
        }
    }
}
