use std::ffi::{c_char, CStr};

#[no_mangle]
pub extern "C" fn add(x: i32, y: i32) -> i32 {
    x + y
}

#[no_mangle]
pub extern "C" fn print_from_go(message: *const c_char) {
    if message.is_null() {
        println!("Received a null pointer!");
        return;
    }

    let c_str = unsafe { CStr::from_ptr(message) };
    match c_str.to_str() {
        Ok(string) => println!("Message from Go: {}", string),
        Err(_) => println!("Failed to convert message from Go to UTF-8"),
    }
}

// Bad idea
// use std::ffi::CString;
//
// #[no_mangle]
// pub extern "C" fn print_from_go(message: CString) {
//     dbg!(message);
// }

use std::os::raw::c_int;

#[repr(C)]
pub struct IntVec {
    data: *const c_int,
    len: usize,
}
#[no_mangle]
pub extern "C" fn get_vector() -> IntVec {
    let vec = vec![10, 20, 30, 40, 50];

    let data = vec.as_ptr();
    let len = vec.len();

    std::mem::forget(vec);

    IntVec { data, len }
}
#[no_mangle]
pub extern "C" fn free_vector(vec: IntVec) {
    if vec.data.is_null() {
        return;
    }
    unsafe {
        Vec::from_raw_parts(vec.data as *mut c_int, vec.len, vec.len);
    }
}