package main

/*
#cgo LDFLAGS: -L./rust_library/target/release -lrust_library
#include <stdint.h>
#include <stdlib.h>

extern int32_t add(int32_t x, int32_t y);
extern void print_from_go(const char* message);

typedef struct {
    const int* data;
    size_t len;
} IntVec;

extern IntVec get_vector();
extern void free_vector(IntVec vec);
*/
import "C"
import "fmt"
import "unsafe"

func main() {
    x, y := 5, 7
    result := C.add(C.int32_t(x), C.int32_t(y))
    fmt.Printf("Result from Rust: %d\n", result)

    message := "Hello from Go!"

    cMessage := C.CString(message)

    C.print_from_go(cMessage)
	defer C.free(unsafe.Pointer(cMessage))
    vec := C.get_vector()

    length := int(vec.len)
    slice := (*[1 << 30]C.int)(unsafe.Pointer(vec.data))[:length:length]

    fmt.Println("Vector from Rust:")
    for _, value := range slice {
        fmt.Printf("%d ", int(value))
    }
    fmt.Println()

    C.free_vector(vec)
}
