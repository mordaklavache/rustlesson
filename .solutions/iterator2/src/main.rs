fn main() {
    let initial_pattern = [1, 2, 3, 4, 5, 7, 10, 11, 20, 21];
    let final_pattern = [2, 1, 4, 3, 7, 5, 11, 10, 21, 20];
    fn check<T>(left: &[T], right: &[T])
    // Generics other examples
    where
        T: std::cmp::PartialEq + std::fmt::Debug,
    {
        assert_eq!(left, right);
    }

    // Solution en 2 instructions avec creation d'une nouvelle collection.
    let arr = initial_pattern;
    let iter = arr.chunks_exact(2).map(|i| [i[1], i[0]]).flatten();
    let v: Vec<_> = iter.collect();

    dbg!(&v);
    check(&v, &final_pattern);

    // Meme chose mais avec une seule instruction.
    let arr = initial_pattern;
    let v: Vec<_> = arr
        .chunks_exact(2)
        .map(|i| [i[1], i[0]])
        .flatten()
        .collect();

    dbg!(&v);
    check(&v, &final_pattern);

    // Solution fonctionnelle avec modification du tableau originale (sans allocation donc).
    let mut arr = initial_pattern;
    arr.chunks_exact_mut(2).for_each(|chunk| chunk.swap(0, 1));

    dbg!(&arr);
    check(&arr, &final_pattern);

    // Alternative a la precedente solution, utilisation de reverse(), plutot que swap().
    let mut arr = initial_pattern;
    arr.chunks_exact_mut(2).for_each(|chunk| chunk.reverse());

    dbg!(&arr);
    check(&arr, &final_pattern);
}
