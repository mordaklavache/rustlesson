#![allow(dead_code)]

fn main() {
    // Implementation naive de la consigne
    {
        #[derive(Debug)]
        struct Auteur {
            nom: String,
        }

        #[derive(Debug)]
        struct Livre<'a> {
            titre: String,
            auteur: &'a Auteur,
        }

        fn creer_livre<'a>(auteur: &'a Auteur, name: &str) -> Livre<'a> {
            Livre {
                titre: name.to_string(),
                auteur,
            }
        }
        fn creer_auteur(name: &str) -> Auteur {
            Auteur {
                nom: name.to_string(),
            }
        }

        // La structure Livre contiendrait ici une reference sur une variable locale de type Auteur !
        // fn creer_livre_et_auteur<'a>(author_name: &str, book_name: &str) -> (Livre<'a>, Auteur) {
        //     let auteur = creer_auteur(author_name);
        //     let livre = creer_livre(&auteur, book_name);
        //     (livre, auteur)
        // }

        let auteur = creer_auteur("Herge");
        dbg!(&auteur);

        let livre = creer_livre(&auteur, "Tintin et Milou");
        dbg!(livre);
    }

    // Resolution du probleme VIA utilisation d'un Reference Counter
    {
        use std::rc::Rc;

        #[derive(Debug)]
        struct Auteur {
            nom: String,
        }

        #[derive(Debug)]
        struct Livre {
            titre: String,
            auteur: Rc<Auteur>,
        }

        fn creer_livre(auteur: Rc<Auteur>, name: &str) -> Livre {
            Livre {
                titre: name.to_string(),
                auteur,
            }
        }
        fn creer_auteur(name: &str) -> Rc<Auteur> {
            Rc::new(Auteur {
                nom: name.to_string(),
            })
        }

        // Fonctionne grace a Rc
        fn creer_livre_et_auteur<'a>(author_name: &str, book_name: &str) -> (Livre, Rc<Auteur>) {
            let auteur = creer_auteur(author_name);
            let livre = creer_livre(auteur.clone(), book_name);
            (livre, auteur)
        }

        let auteur = creer_auteur("Herge");
        dbg!(&auteur);

        let livre = creer_livre(auteur.clone(), "Tintin et Milou");
        dbg!(livre);

        let livre_et_auteur = creer_livre_et_auteur("Victor Hugo", "Les miserables");
        dbg!(&livre_et_auteur);

        let (livre, auteur) = livre_et_auteur;
        dbg!(livre);
        dbg!(auteur);
    }
}

