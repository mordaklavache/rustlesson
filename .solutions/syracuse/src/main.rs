fn syracuse(n: u32) -> u32 {
    println!("{}", n);
    if n == 1 {
        1
    } else if n % 2 == 0 {
        syracuse(n / 2)
    } else {
        syracuse(n * 3 + 1)
    }
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    match args.get(1) {
        Some(arg) => {
            let arg = arg.parse::<u32>().unwrap();
            syracuse(arg);
        }
        None => {
            eprintln!("Aucun argument !");
        }
    }
}
