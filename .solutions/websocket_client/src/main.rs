use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize)]
struct HelloJSON {
    msg: String,
}

fn main() {
    use ws::{connect, CloseCode};

    connect("ws://127.0.0.1:8082", |out| {
        // out.send("Hello WebSocket").unwrap();

        let msg = HelloJSON {
            msg: "Hello From JSON struct".into(),
        };
        out.send(serde_json::to_string(&msg).unwrap()).unwrap();

        move |msg: ws::Message| {
            println!("Got message: {}", &msg);

            let p: HelloJSON = serde_json::from_str(msg.as_text().unwrap()).unwrap();
            dbg!(p);

            out.close(CloseCode::Normal)
        }
    })
    .unwrap()
}
