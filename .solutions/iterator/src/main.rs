fn main() {
    // - Creer une range de 0 a 30 ou 30 est inclu
    // - creer un iterateur dessus
    // - filter seulement les nombre pairs
    // - recuperer l'index
    // - Multiplier par 2 chacun des nonbres recu et ajouter l'index
    // - Placer tout ca dans un vecteur
    // - le tout sur une seule ligne de code

    // Ecriture plus longue afin d'eviter de tomber dans le piege de iter()
    let v: Vec<_> = (1..=30).collect();
    let v: Vec<_> = v
        .iter()
        .filter(|x| *x % 2 == 0)
        .enumerate()
        .map(|(i, x)| x * 2 + i)
        .collect();
    dbg!(v);

    // On laisse le compilateur determiner le type de Vec
    // Ici ce sera un usize
    let v: Vec<_> = (1..=30)
        .filter(|x| *x % 2 == 0)
        .enumerate()
        .map(|(i, x)| x * 2 + i)
        .collect();
    dbg!(v);

    // Si on pense au turbofish dans collect()
    let v = (1..=30)
        .filter(|x| *x % 2 == 0)
        .enumerate()
        .map(|(i, x)| x * 2 + i)
        .collect::<Vec<usize>>();
    dbg!(v);

    // Utiliser un u32 a la place est moins pratique
    // car on doit cast le resultat de map()
    let v = (1..=30)
        .filter(|x| *x % 2 == 0)
        .enumerate()
        .map(|(i, x)| (x * 2 + i) as _)
        .collect::<Vec<u32>>();
    dbg!(v);

    // En utilisant FilterMap instead
    dbg!((1..=30)
        .enumerate()
        .filter_map(|(i, x)| match x % 2 == 0 {
            true => Some(x * 2 + i / 2),
            false => None,
        })
        .collect::<Vec<usize>>());

    // Ecriture fonctionnelle
    let v: Vec<usize> = (0..=30)
        .filter(|elem| elem % 2 == 0)
        .enumerate()
        .map(|(index, elem)| (elem * 2) + index)
        .collect();
    dbg!(v);

    // Ecriture imperative
    const I_BEGIN: usize = 0;
    let mut v = Vec::new();
    let mut i = I_BEGIN;
    while i <= 30 {
        // L'index brut s'obtient par i - I_BEGIN
        if i % 2 == 0 {
            v.push(i * 2 + (i - I_BEGIN) / 2);
        }
        i += 1;
    }
    dbg!(v);
}
