use std::io::Read;

fn read_file(file_name: &str) -> std::io::Result<Vec<u8>> {
    use std::fs::File;

    let mut f = File::open(file_name)?;

    let mut buf = Vec::new();
    f.read_to_end(&mut buf)?;
    Ok(buf)
}

fn main() {
    for (_, file_name) in std::env::args().enumerate().filter(|(idx, _)| *idx != 0) {
        match read_file(&file_name) {
            Ok(buf) => match std::str::from_utf8(&buf) {
                Ok(s) => println!("{}", s),
                Err(e) => eprintln!("incorrect utf8 for {:?} : {:?}", file_name, e),
            }
            Err(e) => eprintln!("error for {:?} : {:?}", file_name, e),
        }
    }
}
