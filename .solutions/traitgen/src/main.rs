fn main() {
    #[derive(PartialEq, PartialOrd)]
    struct Pair<T, U> {
        a: T,
        b: U,
    }

    impl<T: PartialEq, U: PartialEq> Pair<T, U> {
        fn is_equal(&self, other: &Self) -> bool {
            self == other
        }
    }

    impl<T: PartialOrd, U: PartialOrd> Pair<T, U> {
        fn compare(&self, other: &Self) -> Option<std::cmp::Ordering> {
            self.partial_cmp(other)
        }
    }
    let p1 = Pair { a: 42, b: 3.14 };
    let p2 = Pair { a: 42, b: 5.12 };

    dbg!(p1.compare(&p2));
    dbg!(p1.is_equal(&p2));
}
