use std::collections::HashMap;

const ELFIC_BOOTS: &'static str = "Permet de se delacer dans la foret sans faire de bruit.";
const DAGGER: &'static str = "Une simple dague.";
const SWORD: &'static str = "Une simple epee.";
const STAFF: &'static str = "Un vulgaire baton.";

#[derive(Clone, Debug, PartialEq, Eq, Hash)]
enum Slot {
    Hand,
    Feet,
}

#[derive(Debug)]
#[allow(dead_code)]
struct Item {
    kind: Kind,
    description: &'static str,
    slot: Slot,
}

#[derive(Debug)]
#[allow(dead_code)]
struct Character {
    name: String,
    class: Class,
    level: usize,
    inventory: Vec<Item>,
    equipement: HashMap<Slot, Item>,
}

#[derive(Debug, Copy, Clone)]
enum Class {
    Archimage,
    Paladin,
    Voleur,
}

#[derive(Debug)]
enum Weapon {
    Sword,
    Dagger,
    Staff,
}

#[derive(Debug)]
enum Kind {
    Weapon(Weapon),
    Armor,
}

impl Character {
    fn new(name: String, class: Class, level: usize) -> Self {
        Self {
            name,
            class,
            level,
            inventory: Vec::new(),
            equipement: HashMap::new(),
        }
    }
    fn fireball(&self, target: &Self) -> Result<(), String> {
        use Class::*;
        match self.class {
            Archimage => {
                println!("{} has been fireballed by {} !", target.name, self.name);
                Ok(())
            }
            _default => Err("Cannot throw fireball".to_string()),
        }
    }
    fn add_item(&mut self, item: Item) {
        self.inventory.push(item);
    }

    fn equip_item(&mut self, index: usize) {
        let item = self.inventory.remove(index);
        if let Some(item) = self.equipement.insert(item.slot.clone(), item) {
            self.inventory.push(item);
        }
    }
}

fn main() {
    let mut gardakan = Character::new("Gardakan".into(), Class::Paladin, 66);
    let mut mordak = Character::new("Mordak".into(), Class::Archimage, 57);
    let mut bobafett = Character::new("BobaFett".into(), Class::Voleur, 1);
    gardakan.add_item(Item {
        slot: Slot::Hand,
        description: &SWORD,
        kind: Kind::Weapon(Weapon::Sword),
    });
    gardakan.equip_item(0);
    mordak.add_item(Item {
        slot: Slot::Hand,
        description: &STAFF,
        kind: Kind::Weapon(Weapon::Staff),
    });
    mordak.equip_item(0);
    bobafett.add_item(Item {
        slot: Slot::Hand,
        description: &DAGGER,
        kind: Kind::Weapon(Weapon::Dagger),
    });
    bobafett.equip_item(0);

    bobafett.add_item(Item {
        slot: Slot::Feet,
        description: &ELFIC_BOOTS,
        kind: Kind::Armor,
    });
    bobafett.equip_item(0);

    mordak.fireball(&gardakan).unwrap();

    dbg!(&gardakan);
    dbg!(&mordak);
    dbg!(&bobafett);

    mordak.add_item(Item {
        slot: Slot::Hand,
        description: &DAGGER,
        kind: Kind::Weapon(Weapon::Dagger),
    });
    mordak.equip_item(0);
    dbg!(mordak);
}
