pub trait HexDump {
    unsafe fn hex_dump(&self);
}

impl<T> HexDump for T {
    unsafe fn hex_dump(&self) {
        let ptr = std::ptr::addr_of!(*self) as *const u8;
        for byte in 0..std::mem::size_of::<Self>() {
            print!("{:#04x} ", *ptr.offset(byte as isize));
        }
        println!();
    }
}

fn main() {
    let r: u32 = 42;
    unsafe { r.hex_dump() };
    let r: u32 = 0;
    unsafe { r.hex_dump() };
    let r: f64 = 42.12;
    unsafe { r.hex_dump() };
}
