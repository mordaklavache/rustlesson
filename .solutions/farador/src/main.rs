#[derive(Debug)]
#[allow(dead_code)]
struct Character {
    name: String,
    class: Class,
    level: usize,
    weapon: Option<Weapon>,
}

#[derive(Debug, Copy, Clone)]
enum Class {
    Archimage,
    Paladin,
    Voleur,
}

#[derive(Debug)]
enum Weapon {
    Sword,
    Dagger,
    Staff,
}

impl Character {
    fn new(name: String, class: Class, level: usize, weapon: Option<Weapon>) -> Self {
        Self {
            name,
            class,
            level,
            weapon,
        }
    }
    fn fireball(&self) -> Result<(), String> {
        use Class::*;
        match self.class {
            Archimage => Ok(()),
            _default => Err("Cannot throw fireball".to_string()),
        }
    }
    fn take_weapon(&mut self) -> Option<Weapon> {
        self.weapon.take()
    }
}

fn main() {
    let mut gardakan = Character::new("Gardakan".into(), Class::Paladin, 66, Some(Weapon::Sword));
    let mordak = Character::new("Mordak".into(), Class::Archimage, 57, Some(Weapon::Staff));
    let mut bobafett = Character::new("BobaFett".into(), Class::Voleur, 1, Some(Weapon::Dagger));
    gardakan.take_weapon().unwrap();
    mordak.fireball().unwrap();
    bobafett.take_weapon().unwrap();

    dbg!(mordak);
}
