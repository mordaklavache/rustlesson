//! This Rust program is used to perform simple integration
//! tests on programs through a JSON configuration file.    
//!
//! ---
//! After creating a new project with the command `cargo new hello`, create a file named **test.json** like this :
//! ```
//! [
//!     {
//!         "command" : "./target/debug/hello",
//!         "stdout" : "Hello, world!\n"
//!     },
//!     {
//!         "command" : "./target/debug/hello",
//!         "stdout" : "Hello, dummy!\n"
//!     }
//! ]
//! ```
//! Then simply run cargo build then `tiny-integration-tester` :
//! ```
//! Executing command: "./target/debug/hello" with args: []
//! OK!
//! Executing command: "./target/debug/hello" with args: []
//! FAIL!
//! YOU GOT: Output {
//!     status: ExitStatus(
//!         unix_wait_status(
//!             0,
//!         ),
//!     ),
//!     stdout: "Hello, world!\n",
//!     stderr: "",
//! }
//! BUT DESIRED OUTPUT IS: Output {
//!     status: ExitStatus(
//!         unix_wait_status(
//!             0,
//!         ),
//!     ),
//!     stdout: "Hello, dummy!\n",
//!     stderr: "",
//! }
//! result: 1 / 2
//! ```
//! You can set **stdout**, **stderr**, **args** and **timeout** (in ms) like this :
//! ```
//! [
//!     {
//!         "command" : "./fibonacci",
//!         "args" : ["3"],
//!         "stdout" : "fibo(3) = 2\n",
//!         "timeout" : 1000
//!     },
//!     {
//!         "command" : "./fibonacci",
//!         "args" : ["1", ""],
//!         "status" : -1,
//!         "stderr" : "Usage: ./fibonacci POSITIF_NUMBER\n"
//!     },
//!     {
//!         "command" : "./fibonacci",
//!         "args" : ["7"],
//!         "stdout" : "fibo(7) = 13\n"
//!     },
//!     {
//!         "timeout" : 500,
//!         "command" : "./fibonacci",
//!         "args" : ["15"],
//!         "status" : 0,
//!         "stdout" : "fibo(15) = 610\n"
//!     },
//!     {
//!         "command" : "./fibonacci",
//!         "args" : ["164neuf"],
//!         "status" : -1,
//!         "stderr" : "Bad number format\n"
//!     },
//!     {
//!         "command" : "./fibonacci",
//!         "status" : -1,
//!         "stderr" : "Bad number format\n"
//!     }
//! ]
//! ```
//! *Be careful with trailing commas at the end in a JSON file.*

use std::os::unix::process::ExitStatusExt;
use std::process::{exit, Command, Output, Stdio};
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

use colored::Colorize;
use serde::Deserialize;

/// Here's how it is represented on the RUST side.
struct Test {
    command: Command,
    output: Output,
    timeout: Duration,
}

/// For the moment, the Json filename is simply `test.json`.
const JSON_FILENAME: &str = "test.json";

/// Here's how it is represented on the JSON side.
#[derive(Deserialize)]
struct JSONTest<'a> {
    command: &'a str,
    args: Option<Vec<&'a str>>,
    status: Option<i32>,
    stdout: Option<String>,
    stderr: Option<String>,
    timeout: Option<u64>,
}

/// Simply open and read the json file.
fn open_test_file() -> std::io::Result<String> {
    let mut f = std::fs::File::open(JSON_FILENAME)?;
    let mut data = String::new();
    std::io::Read::read_to_string(&mut f, &mut data)?;
    Ok(data)
}

/// Let's look at the main function!
fn main() {
    let content = match open_test_file() {
        Ok(data) => data,
        Err(e) => {
            eprintln!("cannot read json test file `test.json`: err = {}", e);
            exit(-1);
        }
    };

    let json: Vec<JSONTest> = serde_json::from_str(&content).expect("cannot parse json");
    let tests = json
        .iter()
        .map(|context| Test {
            command: {
                let mut command = Command::new(&context.command);
                command
                    .stdout(Stdio::piped())
                    .stderr(Stdio::piped())
                    .args(context.args.clone().unwrap_or([].to_vec()).iter());
                command
            },
            output: Output {
                status: ExitStatusExt::from_raw(
                    context.status.map_or(0, |v| (v as u8 as i32) * (1 << 8)),
                ),
                stdout: context.stdout.clone().unwrap_or("".into()).into(),
                stderr: context.stderr.clone().unwrap_or("".into()).into(),
            },
            timeout: Duration::from_millis(context.timeout.unwrap_or(3000)),
        })
        .collect::<Vec<Test>>();

    let mut success = 0;
    let nb_test = tests.len();
    for mut test in tests {
        println!(
            "Executing command: {:?} with args: {:?}",
            test.command.get_program(),
            test.command
                .get_args()
                .map(|v| v.to_os_string().into_string().unwrap())
                .collect::<Vec<String>>()
        );
        match test.command.spawn() {
            Ok(child) => {
                let (sender, receiver) = mpsc::channel();

                let pid = child.id();
                let killer_thread = thread::spawn(move || {
                    if let Err(_) = receiver.recv_timeout(test.timeout) {
                        unsafe {
                            libc::kill(pid as i32, libc::SIGTERM);
                        }
                    };
                });
                let output = child.wait_with_output().expect("Unexpected error!");
                drop(sender.send(()));
                killer_thread.join().expect("The thread has panicked");
                match output.status.signal() {
                    Some(signal) => {
                        let err_msg = format!("killed by OS signal : {}", signal);
                        eprintln!("{}", err_msg.color("red").on_color("white"));
                    }
                    None => {
                        if output == test.output {
                            success += 1;
                            println!("{}", "OK!".color("green").on_truecolor(30, 30, 30));
                        } else {
                            eprintln!("{}", "FAIL!".color("red").on_color("white"));
                            eprintln!(
                                "YOU GOT: {:#?}\nBUT DESIRED OUTPUT IS: {:#?}",
                                output, test.output
                            );
                        }
                    }
                }
            }
            Err(error) => {
                let err_msg = format!("cannot execute command! err : {}", error);
                eprintln!("{}", err_msg.color("red").on_color("white"));
            }
        }
    }
    let summary = format!("result: {} / {}", success, nb_test);
    println!("{}", summary.color("red").on_color("white"));
}
