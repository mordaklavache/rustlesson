This Rust program is used to perform simple integration tests on programs through a JSON configuration file.  

After creating a new project with the command `cargo new hello`, create a file named **test.json** like this :
```
[
    {
        "command" : "./target/debug/hello",
        "stdout" : "Hello, world!\n"
    },
    {
        "command" : "./target/debug/hello",
        "stdout" : "Hello, dummy!\n"
    }
]
```
Then simply run cargo build then `tiny-integration-tester` :
```
Executing command: "./target/debug/hello" with args: []
OK!
Executing command: "./target/debug/hello" with args: []
FAIL!
YOU GOT: Output {
    status: ExitStatus(
        unix_wait_status(
            0,
        ),
    ),
    stdout: "Hello, world!\n",
    stderr: "",
}
BUT DESIRED OUTPUT IS: Output {
    status: ExitStatus(
        unix_wait_status(
            0,
        ),
    ),
    stdout: "Hello, dummy!\n",
    stderr: "",
}
result: 1 / 2
```
You can set **stdout**, **stderr**, **args** and **timeout** (in ms) like this :
```
[
    {
        "command" : "./fibonacci",
        "args" : ["3"],
        "stdout" : "fibo(3) = 2\n",
        "timeout" : 1000
    },
    {
        "command" : "./fibonacci",
        "args" : ["1", ""],
        "status" : -1,
        "stderr" : "Usage: ./fibonacci POSITIF_NUMBER\n"
    },
    {
        "command" : "./fibonacci",
        "args" : ["7"],
        "stdout" : "fibo(7) = 13\n"
    },
    {
        "timeout" : 500,
        "command" : "./fibonacci",
        "args" : ["15"],
        "status" : 0,
        "stdout" : "fibo(15) = 610\n"
    },
    {
        "command" : "./fibonacci",
        "args" : ["164neuf"],
        "status" : -1,
        "stderr" : "Bad number format\n"
    },
    {
        "command" : "./fibonacci",
        "status" : -1,
        "stderr" : "Bad number format\n"
    }
]
```
*Be careful with trailing commas at the end in a JSON file.*
